/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Record {
    private String EmId;
    private String Name;
    private String telephone;
    private int year;

    public Record(String EmId, String Name, String telephone, int year) {
        this.EmId = EmId;
        this.Name = Name;
        this.telephone = telephone;
        this.year = year;
    }

    public String getEmId() {
        return EmId;
    }

    public void setEmId(String EmId) {
        this.EmId = EmId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public Record(){}
    public List<Record> ReadFile(String path) throws FileNotFoundException {
        try {
            File myObj = new File(path);
            List<Record> records = new ArrayList<>();
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] part = data.split(",");
                Record record = new Record();
                record.setEmId(part[0]);
                record.setName(part[1]);
                record.setTelephone(part[2]);
                record.setYear(Integer.parseInt(part[3]));
                records.add(record);
            }
            
            myReader.close();
            return records;
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            throw new FileNotFoundException();
        }

    }

    public void CreateFile(String path) {
        try {
            File myObj = new File(path);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void WriteToFile(String path, List<Record> record) {
        try {
            FileWriter myWriter = new FileWriter(path);
            String content = "";
            for(Record r : record){
                content = content + r.getEmId() + "," +r.getName()+","+r.getTelephone()+r.getYear() +"\n";// r.getEmId()+","+r.getName()+","+r.getTelephone(),+r.getYear()+"\n";
            }
            myWriter.write(content);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
            
}
